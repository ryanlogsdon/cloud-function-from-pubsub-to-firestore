command without JSON

    gcloud pubsub topics publish MY_TOPIC --message='my message' --attribute='sensorName=sensor-001,temperature=75,humidity=80'

command with JSON

    gcloud pubsub topics publish MY_TOPIC --message='{"sensorLocation":"back yard"}' --attribute='sensorName=sensor-001,temperature=75,humidity=80'